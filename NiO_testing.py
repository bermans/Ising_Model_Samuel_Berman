#!/usr/bin/env python

#Adding second nearest neighbour interactions for NiO 

import numpy as np
import matplotlib.pyplot as plt
import datetime

u1=datetime.datetime.now()
#pick3 chooses a random point in the 3D lattice (built for 3D superlattice)
def pick3(n,N):   
    x=np.random.rand(N,3)
    x1=(x*(n))+n
    x2=x1.astype(int)
    return x2

#############Figure out more efficient way to do energy at each step by just adding dE or doing nothing
#boltzmann constant in ev/K
kb=8.6173303e-5

#J1=nearest neighbours, J2 second nearest neighbours
J1=2.3e-3
J2=-21.0e-3
#print(gr_state)





class lattices:
    'Lattices with side length = size, intial state either random or ordered'
    number=0
    def __init__(self,size):
        self.size=size
        lattices.number += 1
        
        #Make FCC lattice, 0 at points without atoms
        self.superlattice=np.zeros((3*self.size,3*self.size,3*self.size))
        for i in range(3*self.size):
            for j in range(3*self.size):
                for k in range(3*self.size):
                    spin1 = 1
                    if k%2 == 0 and i%2 == 0 and j%2 == 0:
                        self.superlattice[i,j,k]= spin1
                        
                    if k%2 == 1 and i%2 == 0 and j%2 == 1:
                        self.superlattice[i,j,k]= spin1
                        
                    if k%2 == 1 and i%2 == 1 and j%2 == 0:
                        self.superlattice[i,j,k]= spin1
                        
                    if k%2 == 0 and i%2 == 1 and j%2 == 1:
                        self.superlattice[i,j,k]= spin1

         
        sliced1=self.superlattice[self.size:2*self.size,self.size:2*self.size,self.size:2*self.size]

        plt.imshow(sliced1[3],cmap=plt.cm.coolwarm)
        
        self.atoms=np.sum(np.abs(sliced1))
#        print(self.superlattice)
#        print(self.atoms)
#        print(self.size**3)
        print('init done')
    def magnetisation(self):
        sliced=self.superlattice[self.size:2*self.size,self.size:2*self.size,self.size:2*self.size]
        spin_excess=np.sum(sliced)
        M=spin_excess/(self.atoms)
        return M
    
    def plot(self):
        z,x,y=self.superlattice.nonzero()
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.scatter(x, y, -z, zdir='z', c= 'red')

    def inenergy(self):
        yy1=0
        p1=self.size
        q1=self.size
        w1=self.size
        #print(p1,w1,q1)
        for i in range(self.size):
            q1=self.size
            for j in range(self.size):
                w1=self.size
                for k in range(self.size):
                    r1 = self.superlattice[p1,q1+1,w1+1] + self.superlattice[p1,q1+1,w1-1] + self.superlattice[p1,q1-1,w1+1] + self.superlattice[p1,q1-1,w1-1]
        
                    r2 = self.superlattice[p1+1,q1+1,w1] + self.superlattice[p1+1,q1-1,w1] + self.superlattice[p1+1,q1,w1+1] + self.superlattice[p1+1,q1,w1-1]
                    
                    r3 = self.superlattice[p1-1,q1+1,w1] + self.superlattice[p1-1,q1-1,w1] + self.superlattice[p1-1,q1,w1+1] + self.superlattice[p1-1,q1,w1-1]
                    
                    rn = r1+r2+r3
                    
                    Ei1 = (-J1*self.superlattice[p1,q1,w1])*(rn)
                    
                    #second nearest neighbours
                    r2n = self.superlattice[p1+2,q1,w1] + self.superlattice[p1,q1+2,w1] + self.superlattice[p1,q1,w1+2] + self.superlattice[p1-2,q1,w1] + self.superlattice[p1,q1-2,w1] + self.superlattice[p1,q1,w1-2]
                    
                    Ei2 = (-J2*self.superlattice[p1,q1,w1])*(r2n)
                    
                    yy1 = Ei1 + Ei2
                    
                    w1 += 1
                q1 += 1
            p1 += 1
        yy1=yy1/2
        return yy1




#made pick more efficient, now only have to call once, made stats more efficient



    



#metro does the monte carlo algorithm and collects statistics
def metroFCC(N,n,T,start_stats):
#    initial_state='ooooo'
#    if beta<0.4:
#        initial_state='random'
#    else:
#        initial_state='ordered'
    randomlist=np.random.rand(int(3*N),3)
    C=lattices(n)
#    energy = C.inenergy()
    energy = 0
    #print(C.magnetisation())
    #initialize array for statistics
#    mags=np.zeros(np.int(start_stats))
    energies = np.zeros(np.int(start_stats))
    
    dummy1=0
    beta=1/(kb*T)
    
    #not in for now##############################
    #generate random pairs
    #rands=pick3(n,N)
    
    #get random numbers for if statements
    ls=np.random.rand(N)
    i1=0
    for i in range(N):
        #do statistics
        if i%1000000 == 0:
            print('done:',i,datetime.datetime.now())
        if i>=np.int(N-start_stats):

            energies[dummy1] = energy/C.atoms
            
            
            dummy1 += 1
        
        
        
        
        #do metropolis algorithm
        #get random point, p1,q1,w1 are the coordinates of the random point chosen
#        p1=int(np.random.rand()*n + n)
#        q1=int(np.random.rand()*n + n)
#        w1=int(np.random.rand()*n + n)
        p1=int(randomlist[i1,0]*n + n)
        q1=int(randomlist[i1,1]*n + n)
        w1=int(randomlist[i1,2]*n + n)
        
        #if make sure the random point is a lattice point
        while C.superlattice[p1,q1,w1] == 0:
            i1 += 1
            p1=int(randomlist[i1,0]*n + n)
            q1=int(randomlist[i1,1]*n + n)
            w1=int(randomlist[i1,2]*n + n)

        
        
        #calculate energy initial energy
        #FCC coordination number is 12, 4 nn above, 4 in liine, and4 below
        
        #first nearest neighbours
        r1 = C.superlattice[p1,q1+1,w1+1] + C.superlattice[p1,q1+1,w1-1] + C.superlattice[p1,q1-1,w1+1] + C.superlattice[p1,q1-1,w1-1]
        
        r2 = C.superlattice[p1+1,q1+1,w1] + C.superlattice[p1+1,q1-1,w1] + C.superlattice[p1+1,q1,w1+1] + C.superlattice[p1+1,q1,w1-1]
        
        r3 = C.superlattice[p1-1,q1+1,w1] + C.superlattice[p1-1,q1-1,w1] + C.superlattice[p1-1,q1,w1+1] + C.superlattice[p1-1,q1,w1-1]
        
        rn = r1+r2+r3
        
        Ei1 = (-J1*C.superlattice[p1,q1,w1])*(rn)
        
        #second nearest neighbours
        r2n = C.superlattice[p1+2,q1,w1] + C.superlattice[p1,q1+2,w1] + C.superlattice[p1,q1,w1+2] + C.superlattice[p1-2,q1,w1] + C.superlattice[p1,q1-2,w1] + C.superlattice[p1,q1,w1-2]
        
        Ei2 = (-J2*C.superlattice[p1,q1,w1])*(r2n)
        
        Ei = Ei1 + Ei2
        
        #flip the spin
        C.superlattice[p1,q1,w1] = -C.superlattice[p1,q1,w1]
        
        #calculate final energy
        Ef1 = (-J1*C.superlattice[p1,q1,w1])*(rn)
        
        Ef2 = (-J2*C.superlattice[p1,q1,w1])*(r2n)
        
        Ef = Ef1 + Ef2
        #calculate energy difference
        dE = Ef-Ei
        
        if dE > 0:
            f=np.exp(-beta*dE)
            l=ls[i]
            if l < f:
                #keep the spin flipped
                C.superlattice[p1,q1,w1] = C.superlattice[p1,q1,w1]
                energy = energy + dE
            else:
                #flip the spin back
                C.superlattice[p1,q1,w1] = -C.superlattice[p1,q1,w1]
                            
        else:
            #keep the spin flipped
            C.superlattice[p1,q1,w1] = C.superlattice[p1,q1,w1]
            energy = energy + dE
            
        
#        e1=(2*n)-1
#        #check if we are at boundery and if we are set the other points in the superlattice
#        if p1==n or p1==e1 or q1==n or q1==e1 or w1==n or w1==e1:
        #C.setsuperlattice(p1,q1,w1)
        C.superlattice[p1-C.size,q1,w1] = C.superlattice[p1,q1,w1]
        C.superlattice[p1+C.size,q1,w1] = C.superlattice[p1,q1,w1]
        
        C.superlattice[p1,q1-C.size,w1] = C.superlattice[p1,q1,w1]
        C.superlattice[p1,q1+C.size,w1] = C.superlattice[p1,q1,w1]
        
        C.superlattice[p1,q1,w1-C.size] = C.superlattice[p1,q1,w1]
        C.superlattice[p1,q1,w1+C.size] = C.superlattice[p1,q1,w1]
        
        C.superlattice[p1-C.size,q1-C.size,w1] = C.superlattice[p1,q1,w1]
        C.superlattice[p1+C.size,q1+C.size,w1] = C.superlattice[p1,q1,w1]
        C.superlattice[p1+C.size,q1-C.size,w1] = C.superlattice[p1,q1,w1]
        C.superlattice[p1-C.size,q1+C.size,w1] = C.superlattice[p1,q1,w1]
        
        C.superlattice[p1-C.size,q1,w1-C.size] = C.superlattice[p1,q1,w1]
        C.superlattice[p1+C.size,q1,w1+C.size] = C.superlattice[p1,q1,w1]
        C.superlattice[p1+C.size,q1,w1-C.size] = C.superlattice[p1,q1,w1]
        C.superlattice[p1-C.size,q1,w1+C.size] = C.superlattice[p1,q1,w1]
        
        C.superlattice[p1,q1-C.size,w1-C.size] = C.superlattice[p1,q1,w1]
        C.superlattice[p1,q1+C.size,w1+C.size] = C.superlattice[p1,q1,w1]
        C.superlattice[p1,q1+C.size,w1-C.size] = C.superlattice[p1,q1,w1]
        C.superlattice[p1,q1-C.size,w1+C.size] = C.superlattice[p1,q1,w1]
        
        C.superlattice[p1-C.size,q1-C.size,w1-C.size] = C.superlattice[p1,q1,w1]
        C.superlattice[p1+C.size,q1+C.size,w1+C.size] = C.superlattice[p1,q1,w1]
        C.superlattice[p1+C.size,q1+C.size,w1-C.size] = C.superlattice[p1,q1,w1]
        C.superlattice[p1+C.size,q1-C.size,w1-C.size] = C.superlattice[p1,q1,w1]
        C.superlattice[p1+C.size,q1-C.size,w1+C.size] = C.superlattice[p1,q1,w1]
        C.superlattice[p1-C.size,q1+C.size,w1-C.size] = C.superlattice[p1,q1,w1]
        C.superlattice[p1-C.size,q1-C.size,w1+C.size] = C.superlattice[p1,q1,w1]
        C.superlattice[p1-C.size,q1+C.size,w1+C.size] = C.superlattice[p1,q1,w1]
        i1 += 1
#    av_Mag=np.average(mags)
#    av_Mag2=np.average(mags**2)
#    chi=beta*((av_Mag2)-(av_Mag**2))
#    av_Mag=abs(av_Mag)
    av_energy = np.average(energies)
    av_energy2 = np.average(energies**2)    
    pf = 1/(kb*(T**2))
    cvs = pf*((av_energy2)-(av_energy**2))
    print('done %f K' % T)
#    fig1=plt.figure()
    sliced1=C.superlattice[32:64,32:64,32:64]
    
#    fig1=plt.figure()
#    sliced2=sliced1[3]
#    plt.imshow(sliced2,cmap=plt.cm.coolwarm)
#    plt.show()
#    
#    fig2=plt.figure()
#    sliced3=sliced1[4]
#    plt.imshow(sliced3,cmap=plt.cm.coolwarm)
#    plt.show()
#    

	#plot 2D slices
    fig3=plt.figure()
    plt.title('structure %d' % T)
    sliced4=sliced1[5]
    plt.imshow(sliced4,cmap=plt.cm.coolwarm)
    plt.show()
    
    
    fig4=plt.figure()
    plt.title('structure %d' % T)
    sliced5=sliced1[6]
    plt.imshow(sliced5,cmap=plt.cm.coolwarm)
    plt.show()
    
    fig5=plt.figure()
    plt.title('structure %d' % T)
    sliced6=sliced1[7]
    plt.imshow(sliced6,cmap=plt.cm.coolwarm)
    plt.show()
    
#    global gr_state
#    gr_state=copy.deepcopy(sliced1)
    #return mags
#    return av_Mag, chi
    return av_energy, cvs#, sliced2, sliced3

#vmetro can take arrays, outputs 2 arrays,[0] of average magnetisation, [1] of chi
vmetroFCC=np.vectorize(metroFCC,otypes=(float,float))

testt=metroFCC(5000000,32,350.0,1)



#%%
#test=metroFCC(5000000,16,1700.0,1)
#
#
#fig=plt.figure()
#
#plt.imshow(test[2],cmap=plt.cm.coolwarm)
#
#plt.show()
#
#fig=plt.figure()
#
#plt.imshow(test[3],cmap=plt.cm.coolwarm)
#
#plt.show()


samples=40

#temperatures
Ts=np.linspace(1.0,2000.0,samples)
print(Ts)


iters=2000000
start=500000

u=datetime.datetime.now()
test=vmetroFCC(iters,32,Ts,start)



magns=test[0]
chis=test[1]




#np.savetxt('NiO.csv', test, delimiter=',', newline='\n', header='[0] is magnetisation, [1] is chi \n')

fig1=plt.figure()




plt.plot(Ts,magns,'bx')
plt.xlabel('T')
plt.ylabel('E')




fig2=plt.figure()



plt.plot(Ts,chis,'bx')
plt.xlabel('T')
plt.ylabel(r'$C_V$')
#plt.ylabel(r'$\chi$')






u1=datetime.datetime.now()
print(u1-u)
plt.show()



#test=metroFCC(1000000,32,0.01,200000)
#
#xs=np.arange(0,200000)
#
#
#fig1=plt.figure()
#plt.plot(xs,test,'ro',markersize=0.5)
#
#plt.xlabel('Monte Carlo Time')
#plt.ylabel('Magnetisation')
#
#
#
#
#plt.show()

