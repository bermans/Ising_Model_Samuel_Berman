#!/usr/bin/env python

#adding more statistics
#adding more comments


import numpy as np
import lattice as lt
import matplotlib.pyplot as plt
import datetime


u1=datetime.datetime.now()


#pick chooses a random point in the lattice (built for superlattice)
def pick(n,N):   
    x=np.random.rand(N,2)
    x1=(x*(n))+n
    x2=x1.astype(int)
    return x2



class lattices:
    'Lattices with side length = size, intial state either random or ordered'
    number=0
    #intialise lattice
    def __init__(self,size,initial_state):
        self.size=size
        self.initial_state=initial_state
        lattices.number += 1
        
        #choose initial state
        if self.initial_state == 'random':
            superlattice=lt.gen_rand_lattice(size)
        else:
            superlattice=np.ones((3*size,3*size))
        self.superlattice=superlattice
        
        
    def magnetisation(self):
        #slice out the central lattice of the superlattice
        sliced=self.superlattice[self.size:2*self.size,self.size:2*self.size]
        #calculate spin excess
        spin_excess=np.sum(sliced)
        M=spin_excess/(self.size**2)
        return M
        
    










#made pick more efficient, now only have to call once, made stats more efficient

J=1


    



#metro does the monte carlo algorithm and collects statistics (square lattice), returns the average magnetization and chi
def metro(N,n,beta,initial_state,start_stats):
    
    C=lattices(n,initial_state)
    #initialize array for statistics
    mags=np.zeros(np.int(start_stats))
    energy=0
    energies = np.zeros(np.int(start_stats))
    w1=0
    
    #generate random pairs
    rands=pick(n,N)
    
    #get random numbers for if statements
    ls=np.random.rand(N)
    
    for i in range(N):
        
        #do statistics
        if i>=np.int(N-start_stats):
            mags[w1]=C.magnetisation()
            energies[w1] = energy/(n**2)
            
            
            w1 += 1
        
        
        
        
        #do metropolis algorithm
        #get random point
        p1=rands[i,0]
        q1=rands[i,1]
        
        
        #calculate energy initial energy
        r = C.superlattice[p1+1,q1] + C.superlattice[p1-1,q1] + C.superlattice[p1,q1+1] + C.superlattice[p1,q1-1]
        Ei = (-J*C.superlattice[p1,q1])*(r)
        
        #flip the spin
        C.superlattice[p1,q1] = -C.superlattice[p1,q1]
        
        #calculate final energy
        Ef = (-J*C.superlattice[p1,q1])*(r)
        
        #calculate energy difference
        dE = Ef-Ei
        
        if dE > 0:
            f=np.exp(-beta*dE)
            l=ls[i]
            if l < f:
                #keep the spin flipped
                C.superlattice[p1,q1] = C.superlattice[p1,q1]
                energy = energy + dE
            else:
                #flip the spin back
                C.superlattice[p1,q1] = -C.superlattice[p1,q1]
                            
        else:
            #keep the spin flipped
            C.superlattice[p1,q1] = C.superlattice[p1,q1]
            energy = energy + dE
        
#        if i % 1000000 == 0:
#            sliced1=C.superlattice[n:2*n,n:2*n]
#            lt.plot_lattice(sliced1,beta)
        
        
        e1=(2*n)-1
        #check if we are at boundery and if we are set the other points in the superlattice
        if p1==n or p1==e1 or q1==n or q1==e1:
            C.superlattice[p1-n,q1] = C.superlattice[p1,q1]
            C.superlattice[p1,q1-n] = C.superlattice[p1,q1]
            C.superlattice[p1+n,q1] = C.superlattice[p1,q1]
            C.superlattice[p1,q1+n] = C.superlattice[p1,q1]
            
        else:
            pass
        
    #plot the lattice
#    sliced1=C.superlattice[n:2*n,n:2*n]
#    lt.plot_lattice(sliced1,beta)
    
    av_Mag=np.average(mags)
    av_Mag2=np.average(mags**2)
    chi=beta*((av_Mag2)-(av_Mag**2))
    av_Mag=abs(av_Mag)
    
    av_energy = np.average(energies)
    av_energy2 = np.average(energies**2)    
    pf = beta**2
    cvs = pf*((av_energy2)-(av_energy**2))
    
    #testing
    print('done %f' % beta)
    
#    tt=np.arange(0,start_stats)
#    fig=plt.figure()
#    plt.plot(tt,mags,'gx')
    
    
    
    return av_Mag,chi,av_energy,cvs
    #return mags, av_Mag, chi
    
    
    
#vmetro can take arrays, outputs 2 arrays,[0] of average magnetisation, [1] of chi
vmetro=np.vectorize(metro,otypes=(float,float,float,float))























#honeymetro does the monte carlo algorithm and collects statistics (honeycomb lattice), returns the average magnetization and chi
def honeymetro(N,n,beta,initial_state,start_stats):

    
    C=lattices(n,initial_state)
    #initialize array for statistics
    mags=np.zeros(np.int(start_stats))
    energy=0
    energies = np.zeros(np.int(start_stats))
    w1=0
    
    #generate random pairs
    rands=pick(n,N)
    
    #get random numbers for if statements
    ls=np.random.rand(N)
    
    for i in range(N):
        
        #do statistics
        if i>=np.int(N-start_stats):
            mags[w1]=C.magnetisation()
            energies[w1] = energy/(n**2)
           
            
            
            w1 += 1
        
        
        
        
        #do metropolis algorithm
        #get random point
        p1=rands[i,0]
        q1=rands[i,1]
        
        
        #calculate energy initial energy, check if even site or odd site for nearest neighbours
        tt=p1+q1
        if tt%2 == 0:
            r = C.superlattice[p1+1,q1] + C.superlattice[p1-1,q1] + C.superlattice[p1,q1+1]
        else:
            r = C.superlattice[p1+1,q1] + C.superlattice[p1-1,q1] + C.superlattice[p1,q1-1]
            
            
        Ei = (-J*C.superlattice[p1,q1])*(r)
        
        #flip the spin
        C.superlattice[p1,q1] = -C.superlattice[p1,q1]
        
        #calculate final energy
        Ef = (-J*C.superlattice[p1,q1])*(r)
        
        #calculate energy difference
        dE = Ef-Ei
        
        if dE > 0:
            f=np.exp(-beta*dE)
            l=ls[i]
            if l < f:
                #keep the spin flipped
                C.superlattice[p1,q1] = C.superlattice[p1,q1]
                energy = energy + dE
            else:
                #flip the spin back
                C.superlattice[p1,q1] = -C.superlattice[p1,q1]
                            
        else:
            #keep the spin flipped
            C.superlattice[p1,q1] = C.superlattice[p1,q1]
            energy = energy + dE
        
#        if i % 1000000 == 0:
#            sliced1=C.superlattice[n:2*n,n:2*n]
#            lt.plot_lattice(sliced1,beta)
        
        

        #set the other points in the superlattice

        C.superlattice[p1-n,q1] = C.superlattice[p1,q1]
        C.superlattice[p1,q1-n] = C.superlattice[p1,q1]
        C.superlattice[p1+n,q1] = C.superlattice[p1,q1]
        C.superlattice[p1,q1+n] = C.superlattice[p1,q1]
        C.superlattice[p1+n,q1+n] = C.superlattice[p1,q1]
        C.superlattice[p1-n,q1-n] = C.superlattice[p1,q1]
        C.superlattice[p1-n,q1+n] = C.superlattice[p1,q1]
        C.superlattice[p1+n,q1-n] = C.superlattice[p1,q1]
    #plot the lattice
    #sliced1=C.superlattice[n:2*n,n:2*n]
    #lt.plot_lattice(C.superlattice,beta)
    
    av_Mag=np.average(mags)
    av_Mag2=np.average(mags**2)
    chi=beta*((av_Mag2)-(av_Mag**2))
    av_Mag=abs(av_Mag)
    
    av_energy = np.average(energies)
    av_energy2 = np.average(energies**2)    
    pf = beta**2
    cvs = pf*((av_energy2)-(av_energy**2))
    
    
    
    #testing
    print('done %f' % beta)
    
#    tt=np.arange(0,start_stats)
#    fig=plt.figure()
#    plt.plot(tt,mags,'gx')

    
    return av_Mag,chi,av_energy,cvs
    #return mags, av_Mag, chi
    
    
    
#vmetro can take arrays, outputs 2 arrays,[0] of average magnetisation, [1] of chi
vhoneymetro=np.vectorize(honeymetro,otypes=(float,float,float,float))

















##metro does the monte carlo algorithm and collects statistics (triangular lattice), returns the average magnetization and chi

def trimetro(N,n,beta,initial_state,start_stats):

    
    C=lattices(n,initial_state)
    #initialize array for statistics
    mags=np.zeros(np.int(start_stats))
    
    w1=0
    energy=0
    energies=np.zeros(np.int(start_stats))
    #generate random pairs
    rands=pick(n,N)
    
    #get random numbers for if statements
    ls=np.random.rand(N)
    
    for i in range(N):
        
        #do statistics
        if i>=np.int(N-start_stats):
            mags[w1]=C.magnetisation()
            energies[w1] = energy/(n**2)
            
            
            w1 += 1
        
        
        
        
        #do metropolis algorithm
        #get random point
        p1=rands[i,0]
        q1=rands[i,1]
        
        
        #calculate energy initial energy
        r = C.superlattice[p1+1,q1] + C.superlattice[p1-1,q1] + C.superlattice[p1,q1+1] + C.superlattice[p1,q1-1] + C.superlattice[p1-1,q1+1] + C.superlattice[p1+1,q1-1]
        Ei = (-J*C.superlattice[p1,q1])*(r)
        
        #flip the spin
        C.superlattice[p1,q1] = -C.superlattice[p1,q1]
        
        #calculate final energy
        Ef = (-J*C.superlattice[p1,q1])*(r)
        
        #calculate energy difference
        dE = Ef-Ei
        
        if dE > 0:
            f=np.exp(-beta*dE)
            l=ls[i]
            if l < f:
                #keep the spin flipped
                C.superlattice[p1,q1] = C.superlattice[p1,q1]
                energy = energy + dE
            else:
                #flip the spin back
                C.superlattice[p1,q1] = -C.superlattice[p1,q1]
                            
        else:
            #keep the spin flipped
            C.superlattice[p1,q1] = C.superlattice[p1,q1]
            energy = energy + dE
#        if i % 1000000 == 0:
#            sliced1=C.superlattice[n:2*n,n:2*n]
#            lt.plot_lattice(sliced1,beta)
        
        

        #set the other points in the superlattice

        C.superlattice[p1-n,q1] = C.superlattice[p1,q1]
        C.superlattice[p1,q1-n] = C.superlattice[p1,q1]
        C.superlattice[p1+n,q1] = C.superlattice[p1,q1]
        C.superlattice[p1,q1+n] = C.superlattice[p1,q1]
        C.superlattice[p1+n,q1+n] = C.superlattice[p1,q1]
        C.superlattice[p1-n,q1-n] = C.superlattice[p1,q1]
        C.superlattice[p1-n,q1+n] = C.superlattice[p1,q1]
        C.superlattice[p1+n,q1-n] = C.superlattice[p1,q1]
    #plot the lattice
    #sliced1=C.superlattice[n:2*n,n:2*n]
    #lt.plot_lattice(C.superlattice,beta)
    
    av_Mag=np.average(mags)
    av_Mag2=np.average(mags**2)
    chi=beta*((av_Mag2)-(av_Mag**2))
    av_Mag=abs(av_Mag)
    
    av_energy = np.average(energies)
    av_energy2 = np.average(energies**2)    
    pf = beta**2
    cvs = pf*((av_energy2)-(av_energy**2))
    
    #testing
    print('done %f' % beta)
    
#    tt=np.arange(0,start_stats)
#    fig=plt.figure()
#    plt.plot(tt,mags,'gx')

    
    return av_Mag,chi,av_energy,cvs
    #return mags, av_Mag, chi
    
    
    
#vmetro can take arrays, outputs 2 arrays,[0] of average magnetisation, [1] of chi
vtrimetro=np.vectorize(trimetro,otypes=(float,float,float,float))
