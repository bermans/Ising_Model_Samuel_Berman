#!/usr/bin/env python
#1D case

import numpy as np
import matplotlib.pyplot as plt




def pick1D(n,N):   
    x=np.random.rand(N)
    x=x*n
    x=x.astype(int)
    return x


def metro1D(N,n,beta,start_stats):
    J=1
    mags=np.zeros(start_stats)
    
    
    
    
    #make random arrangement
    #choices=np.array([1,-1])
#    sl=np.random.choice(choices,n)
#    C=sl
    #print(C)
    
    #
    
    
    C=np.ones(n)
    dummy1=0
    
    ps=pick1D(n,N)
    
    for i in range(N):
        
        #collect stats if 
        if i>=np.int(N-start_stats):
            mags[dummy1]=np.sum(C)/n

            
            
            dummy1 += 1
        
        
        
        
        
        p1=ps[i]
        
        #if at boundry 
        if p1==0:
            r=C[1]+C[n-1]
        if p1==(n-1):
            r=C[n-2]+C[0]
            
        #if not at boundry
        else:
            r=C[p1+1]+C[p1-1]
    
    
        #cacluate hamiltonian
        Ei=(-J*C[p1])*(r)
        C[p1]=-C[p1]
        Ef=(-J*C[p1])*(r)
        dE=Ef-Ei
        
        if dE > 0:
            f=np.exp(-beta*dE)
            l=np.random.rand(1)
            if l < f:
                C[p1]=C[p1]
            else:
                C[p1]=-C[p1]
                            
        else:
            C[p1]=C[p1]
    
#    fig1=plt.figure()
#    us=np.arange(0,len(mags),1)
#    plt.plot(us,mags,'gx')
#    plt.show()
#    
    
    
    
    av_Mag=np.average(mags)
    av_Mag=np.abs(av_Mag)
    return av_Mag


#make metro accept arrays
vmetro1D=np.vectorize(metro1D)






#number of data points
samples1=10

#temperatures
Ts1=np.linspace(0.01,3.0,samples1)

#betas
bs1=1/Ts1



test1=vmetro1D(100000,32,bs1,50000)

fig1=plt.figure()


plt.plot(Ts1,test1,'bx')
plt.xlabel('Temperature')
plt.ylabel('Magnetisation')

plt.show()






