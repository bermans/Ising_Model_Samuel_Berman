#!/usr/bin/env python

#made gen_rand_lattice more efficient, removed for loops


import numpy as np
import matplotlib.pyplot as plt
import datetime

t1=datetime.datetime.now()
m1=t1.minute
s1=t1.second




#gen_rand_lattice generates a random nxm spin lattice  by iterating through all entries in the
    #middle block and generating a random number


def gen_rand_lattice(n):

    choices=np.array([1,-1])
    
    sl=np.random.choice(choices,(n,n))
    
    
    sl=sl.astype(int)
    
    x1=np.append(sl,sl,0)
    x1=np.append(x1,sl,0)
    
    x2=np.append(x1,x1,1)
    x2=np.append(x2,x1,1)
    
    return x2

def gen_rand_lattice3D(n):

    choices=np.array([1,-1])
    
    sl=np.random.choice(choices,(n,n,n))
    
    
    sl=sl.astype(int)

    
    x1=np.append(sl,sl,0)
    x1=np.append(x1,sl,0)
    
    x2=np.append(x1,x1,1)
    x2=np.append(x2,x1,1)
    
    x3=np.append(x2,x2,2)
    x3=np.append(x3,x2,2)
    
    return x3



#
def plot_lattice2D(A,k):
    fig1=plt.figure()
    plt.title('%d' % k)
    plt.imshow(A,cmap=plt.cm.BuPu_r)
    plt.show()
    return fig1



