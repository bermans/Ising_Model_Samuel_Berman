#!/usr/bin/env python

#calculating observables for 3D lattice structures



import numpy as np
import matplotlib.pyplot as plt
import montecarlo3D as mc
import datetime

starttime=datetime.datetime.now()


samples=20

#temperatures
Ts=np.linspace(1.0,14.0,samples)

#betas
bs=1/Ts

iters=3000000
start=300000

#simple cubic
runsc=mc.vmetro3D(iters,32,bs,'sc',start)
magns_sc=runsc[0]
chis_sc=runsc[1]
ener_sc=runsc[2]
cvs_sc=runsc[3]

#bcc
runbcc=mc.vmetro3D(iters,32,bs,'bcc',start)
magns_bcc=runbcc[0]
chis_bcc=runbcc[1]
ener_bcc=runbcc[2]
cvs_bcc=runbcc[3]

#fcc
runfcc=mc.vmetro3D(iters,32,bs,'fcc',start)
magns_fcc=runfcc[0]
chis_fcc=runfcc[1]
ener_fcc=runfcc[2]
cvs_fcc=runfcc[3]


#save the data as csv
np.savetxt('data_sc1.csv', runsc, delimiter=',', newline='\n', header='[0] is magnetisation, [1] is chi \n')
np.savetxt('data_bcc1.csv', runbcc, delimiter=',', newline='\n', header='[0] is magnetisation, [1] is chi \n')
np.savetxt('data_fcc1.csv', runfcc, delimiter=',', newline='\n', header='[0] is magnetisation, [1] is chi \n')


sc_data=np.genfromtxt('data_sc1.csv',delimiter=',')
bcc_data=np.genfromtxt('data_bcc1.csv', delimiter=',')
fcc_data=np.genfromtxt('data_fcc1.csv', delimiter=',')

#set the data
magssc=sc_data[0]
chisc=sc_data[1]
enersc=sc_data[2]
cvsc=sc_data[3]

magsbcc=bcc_data[0]
chibcc=bcc_data[1]
enerbcc=bcc_data[2]
cvbcc=bcc_data[3]

magsfcc=fcc_data[0]
chifcc=fcc_data[1]
enerfcc=fcc_data[2]
cvfcc=fcc_data[3]

#plot the data
###############################
fig1=plt.figure(figsize=(4,3))
plt.plot(Ts,magssc,'gx')
plt.plot(Ts,magsfcc,'rx')
plt.plot(Ts,magsbcc,'bx')

plt.title('Magnetisation vs Temperature')

plt.xlabel(r'$T$')
plt.ylabel(r'$M$')

##############################
fig2=plt.figure(figsize=(4,3))
plt.plot(Ts,chisc,'gx')
plt.plot(Ts,chifcc,'rx')
plt.plot(Ts,chibcc,'bx')

plt.title('Magnetic Susceptibility vs Temperature')


plt.xlabel(r'$T$')
plt.ylabel(r'$\chi$')

#################################
fig3=plt.figure(figsize=(4,3))
plt.plot(Ts,enersc,'gx')
plt.plot(Ts,enerfcc,'rx')
plt.plot(Ts,enerbcc,'bx')

plt.title('Energy vs Temperature')


plt.xlabel(r'$T$')
plt.ylabel(r'Energy')
#################
fig4=plt.figure(figsize=(4,3))
plt.plot(Ts,cvsc,'gx')
plt.plot(Ts,cvfcc,'rx')
plt.plot(Ts,cvbcc,'bx')

plt.title('Heat Capacity vs Temperature')


plt.xlabel(r'$T$')
plt.ylabel(r'$C_V$')


endtime=datetime.datetime.now()
print(endtime-starttime)

plt.show()