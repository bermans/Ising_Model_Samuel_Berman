#!/usr/bin/env python
import numpy as np
import datetime

#module for creating pbm files from numpy arrays


now = datetime.datetime.now()


#added date/time to file name

#mk1 generates pbm file for input array A, with filename 'name' ## or name #with date
def makepbm(A,name):
	#f=open(name % (now.minute,now.hour,now.day,now.month,now.year), 'w')
	f=open(name, 'w')
	space= " " 

	f.write("P1\n")

	f.write("%d %d\n" % (A.shape[1],A.shape[0]))
	R=A.shape[0]-1

	for j in range(A.shape[0]):

		for i in range(A.shape[1]):
			q1= "%d" % A[j,i]
			f.write(q1)
			f.write(space)
			if i==R:
				f.write("\n")


	f.close()



