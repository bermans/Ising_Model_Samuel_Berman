#!/usr/bin/env python

#calculating observables for 2D lattice structures

import numpy as np
import matplotlib.pyplot as plt
import datetime
import montecarlo2D


start = datetime.datetime.now()

#sa=number of data points
sa=40


Ts=np.linspace(0.2,6.0,sa)
bs=1/Ts



sqrun=montecarlo2D.vmetro(1000000,32,bs,'ordered',100000)

honeyrun=montecarlo2D.vhoneymetro(1000000,32,bs,'ordered',100000)

trirun=montecarlo2D.vtrimetro(1000000,32,bs,'ordered',100000)




###################################################
magn_honey=honeyrun[0]

chis_honey=honeyrun[1]

ener_honey=honeyrun[2]

cvss_honey=honeyrun[3]


np.savetxt('honeycomb.csv',honeyrun,delimiter=',')


###################################################
magn_sq=sqrun[0]

chis_sq=sqrun[1]

ener_sq=sqrun[2]

cvss_sq=sqrun[3]


np.savetxt('square.csv',sqrun,delimiter=',')

#####################################################

magn_tri=trirun[0]

chis_tri=trirun[1]

ener_tri=trirun[2]

cvss_tri=trirun[3]


np.savetxt('tri.csv',trirun,delimiter=',')










#green square, red tri, blue honey

fig1=plt.figure(figsize=(4,3))
plt.plot(Ts,magn_tri,'rx')
plt.plot(Ts,magn_honey,'yx')
plt.plot(Ts,magn_sq,'bx')


plt.title('Magnetisation vs Temperature')

plt.xlabel(r'$T$')
plt.ylabel(r'$M$')



fig2=plt.figure(figsize=(4,3))
plt.plot(Ts,chis_tri,'rx')
plt.plot(Ts,chis_honey,'yx')
plt.plot(Ts,chis_sq,'bx')


plt.title('Magnetic Susceptibility vs Temperature')


plt.xlabel(r'$T$')
plt.ylabel(r'$\chi$')



#energy
fig3=plt.figure(figsize=(4,3))
plt.plot(Ts,ener_tri,'rx')
plt.plot(Ts,ener_honey,'yx')
plt.plot(Ts,ener_sq,'bx')


plt.title('Energy vs Temperature')


plt.xlabel(r'$T$')
plt.ylabel(r'Energy')


#heat capacity
fig4=plt.figure(figsize=(4,3))
plt.plot(Ts,cvss_tri,'rx')
plt.plot(Ts,cvss_honey,'yx')
plt.plot(Ts,cvss_sq,'bx')


plt.title('Heat Capacity vs Temperature')


plt.xlabel(r'$T$')
plt.ylabel(r'$C_V$')

end = datetime.datetime.now()
print(end - start)




plt.show()