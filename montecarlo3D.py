#!/usr/bin/env python


import numpy as np
import matplotlib.pyplot as plt






#pick3 chooses a random point in the 3D lattice (built for 3D superlattice)
def pick3(n,N):   
    x=np.random.rand(N,3)
    x1=(x*(n))+n
    x2=x1.astype(int)
    return x2






class lattices:
    'Lattices with side length = size, intial state either random or ordered'
    number=0
    def __init__(self,size,structure):
        self.size=size
        
        self.structure=structure
        lattices.number += 1
        #initial lattice structure depending on input
        if self.structure == 'sc':
            self.superlattice=np.ones((3*size,3*size,3*size))
            
        if self.structure == 'fcc':
            self.superlattice=np.zeros((3*self.size,3*self.size,3*self.size))
        
            #Make FCC lattice, 0 at points without atoms
            for i in range(3*self.size):
                for j in range(3*self.size):
                    for k in range(3*self.size):
                        if k%2 == 0 and i%2 == 0 and j%2 == 0:
                            self.superlattice[i,j,k]=1
                        if k%2 == 1 and i%2 == 0 and j%2 == 1:
                            self.superlattice[i,j,k]=1
                        if k%2 == 1 and i%2 == 1 and j%2 == 0:
                            self.superlattice[i,j,k]=1
                        if k%2 == 0 and i%2 == 1 and j%2 == 1:
                            self.superlattice[i,j,k]=1
                            
        if self.structure == 'bcc':
            self.superlattice=np.zeros((3*self.size,3*self.size,3*self.size))
        
            #Make BCC lattice, 0 at points without atoms
            for i in range(3*self.size):
                for j in range(3*self.size):
                    for k in range(3*self.size):
                        if k%2 == 0 and i%2 == 0 and j%2 == 0:
                            self.superlattice[i,j,k]=1
                        if k%2 == 1 and i%2 == 1 and j%2 == 1:
                            self.superlattice[i,j,k]=1                
                            
        sliced1=self.superlattice[self.size:2*self.size,self.size:2*self.size,self.size:2*self.size]
        
        #atoms=number of lattice points
        self.atoms=np.sum(sliced1)                 
                            
        
        
    def magnetisation(self):
        sliced=self.superlattice[self.size:2*self.size,self.size:2*self.size,self.size:2*self.size]
        spin_excess=np.sum(sliced)
        M=spin_excess/(self.atoms)
        return M
        
    def nearest_neighbour_product(self,x,y,z):
        #calculate the nearest neighbour product for energy depending on crystal structure
        if self.structure == 'sc':
            
            r1 = self.superlattice[x+1,y,z] + self.superlattice[x-1,y,z] + self.superlattice[x,y+1,z] + self.superlattice[x,y-1,z] + self.superlattice[x,y,z+1] + self.superlattice[x,y,z-1]
        
        if self.structure == 'fcc':
            #4 in line
            r11 = self.superlattice[x,y+1,z+1] + self.superlattice[x,y+1,z-1] + self.superlattice[x,y-1,z+1] + self.superlattice[x,y-1,z-1]
            
            #4 above
            r21 = self.superlattice[x+1,y+1,z] + self.superlattice[x+1,y-1,z] + self.superlattice[x+1,y,z+1] + self.superlattice[x+1,y,z-1]
            
            #4 below
            r31 = self.superlattice[x-1,y+1,z] + self.superlattice[x-1,y-1,z] + self.superlattice[x-1,y,z+1] + self.superlattice[x-1,y,z-1]
            
            r1 = r11+r21+r31
           
        if self.structure == 'bcc':
            r11 = self.superlattice[x-1,y+1,z+1] + self.superlattice[x-1,y+1,z-1] + self.superlattice[x-1,y-1,z+1] + self.superlattice[x-1,y-1,z-1]
            
            r21 = self.superlattice[x+1,y+1,z+1] + self.superlattice[x+1,y+1,z-1] + self.superlattice[x+1,y-1,z+1] + self.superlattice[x+1,y-1,z-1]
            
            r1 = r11+r21
            
        return r1
            
            
    def setsuperlattice(self,p1,q1,w1):
        #set all points in 3D superlattice
        self.superlattice[p1-self.size,q1,w1] = self.superlattice[p1,q1,w1]
        self.superlattice[p1+self.size,q1,w1] = self.superlattice[p1,q1,w1]
        
        self.superlattice[p1,q1-self.size,w1] = self.superlattice[p1,q1,w1]
        self.superlattice[p1,q1+self.size,w1] = self.superlattice[p1,q1,w1]
        
        self.superlattice[p1,q1,w1-self.size] = self.superlattice[p1,q1,w1]
        self.superlattice[p1,q1,w1+self.size] = self.superlattice[p1,q1,w1]
        
        self.superlattice[p1-self.size,q1-self.size,w1] = self.superlattice[p1,q1,w1]
        self.superlattice[p1+self.size,q1+self.size,w1] = self.superlattice[p1,q1,w1]
        self.superlattice[p1+self.size,q1-self.size,w1] = self.superlattice[p1,q1,w1]
        self.superlattice[p1-self.size,q1+self.size,w1] = self.superlattice[p1,q1,w1]
        
        self.superlattice[p1-self.size,q1,w1-self.size] = self.superlattice[p1,q1,w1]
        self.superlattice[p1+self.size,q1,w1+self.size] = self.superlattice[p1,q1,w1]
        self.superlattice[p1+self.size,q1,w1-self.size] = self.superlattice[p1,q1,w1]
        self.superlattice[p1-self.size,q1,w1+self.size] = self.superlattice[p1,q1,w1]
        
        self.superlattice[p1,q1-self.size,w1-self.size] = self.superlattice[p1,q1,w1]
        self.superlattice[p1,q1+self.size,w1+self.size] = self.superlattice[p1,q1,w1]
        self.superlattice[p1,q1+self.size,w1-self.size] = self.superlattice[p1,q1,w1]
        self.superlattice[p1,q1-self.size,w1+self.size] = self.superlattice[p1,q1,w1]
        
        self.superlattice[p1-self.size,q1-self.size,w1-self.size] = self.superlattice[p1,q1,w1]
        self.superlattice[p1+self.size,q1+self.size,w1+self.size] = self.superlattice[p1,q1,w1]
        self.superlattice[p1+self.size,q1+self.size,w1-self.size] = self.superlattice[p1,q1,w1]
        self.superlattice[p1+self.size,q1-self.size,w1-self.size] = self.superlattice[p1,q1,w1]
        self.superlattice[p1+self.size,q1-self.size,w1+self.size] = self.superlattice[p1,q1,w1]
        self.superlattice[p1-self.size,q1+self.size,w1-self.size] = self.superlattice[p1,q1,w1]
        self.superlattice[p1-self.size,q1-self.size,w1+self.size] = self.superlattice[p1,q1,w1]
        self.superlattice[p1-self.size,q1+self.size,w1+self.size] = self.superlattice[p1,q1,w1]
        









#made pick more efficient, now only have to call once, made stats more efficient

J=1


    



#metro does the monte carlo algorithm and collects statistics
def metro3D(N,n,beta,crystal_structure,start_stats):
    
    
    C=lattices(n,crystal_structure)
    #initialize array for statistics
    mags=np.zeros(np.int(start_stats))
    
    dummy1=0
    energy=0
    energies=np.zeros(np.int(start_stats))
    
    #generate list of random numbers  (5*N) becuase not all random points will be lattice points
    randomlist=np.random.rand(int(5*N),3)
    i1=0
    
    
    #get more random numbers for if statements
    ls=np.random.rand(N)
    
    for i in range(N):
        
        #do statistics
        if i>=np.int(N-start_stats):
            mags[dummy1]=C.magnetisation()
            energies[dummy1] = energy/C.atoms

            
            
            dummy1 += 1
        
        
        
        
        #do metropolis algorithm
        #get random point, p1,q1,w1 are the coordinates of the random point chosen
        p1=int(randomlist[i1,0]*n + n)
        q1=int(randomlist[i1,1]*n + n)
        w1=int(randomlist[i1,2]*n + n)
        
        #if make sure the random point is a lattice point
        while C.superlattice[p1,q1,w1] == 0:
            i1 += 1
            p1=int(randomlist[i1,0]*n + n)
            q1=int(randomlist[i1,1]*n + n)
            w1=int(randomlist[i1,2]*n + n)
        
        #calculate energy initial energy
        r = C.nearest_neighbour_product(p1,q1,w1)
        Ei = (-J*C.superlattice[p1,q1,w1])*(r)
        
        #flip the spin
        C.superlattice[p1,q1,w1] = -C.superlattice[p1,q1,w1]
        
        #calculate final energy
        Ef = (-J*C.superlattice[p1,q1,w1])*(r)
        
        #calculate energy difference
        dE = Ef-Ei
        
        if dE > 0:
            f=np.exp(-beta*dE)
            l=ls[i]
            if l < f:
                #keep the spin flipped
                C.superlattice[p1,q1,w1] = C.superlattice[p1,q1,w1]
                energy = energy + dE
            else:
                #flip the spin back
                C.superlattice[p1,q1,w1] = -C.superlattice[p1,q1,w1]
                            
        else:
            #keep the spin flipped
            C.superlattice[p1,q1,w1] = C.superlattice[p1,q1,w1]
            energy = energy + dE
        
        
        #set the other points in the superlattice
        C.setsuperlattice(p1,q1,w1)
        i1 += 1
    
    #calculate stats
    av_Mag=np.average(mags)
    av_Mag2=np.average(mags**2)
    chi=beta*((av_Mag2)-(av_Mag**2))
    av_Mag=abs(av_Mag)
    
    av_energy = np.average(energies)
    av_energy2 = np.average(energies**2)    
    pf = beta**2
    cvs = pf*((av_energy2)-(av_energy**2))
    
    
    print('done %f' % beta)
    
    #return mags
    return av_Mag, chi, av_energy, cvs
    

#vmetro can take arrays, outputs 2 arrays,[0] of average magnetisation, [1] of chi
vmetro3D=np.vectorize(metro3D,otypes=(float,float,float,float))

